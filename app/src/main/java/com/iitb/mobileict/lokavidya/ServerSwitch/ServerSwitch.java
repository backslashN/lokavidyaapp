package com.iitb.mobileict.lokavidya.ServerSwitch;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.iitb.mobileict.lokavidya.Communication.Settings;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by root on 7/4/16.
 */
public class ServerSwitch {
    public static String[] serverURLS = {"ruralict.cse.iitb.ac.in/lokavidya","ruralict.cse.iitb.ac.in/lokavidya" };

    public static void setServerURLS(Context context)
    {
        final ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = connMgr.getActiveNetworkInfo();
        boolean isReachable = false;
        for(int i=0;i<serverURLS.length;i++) {
            isReachable = false;
            if (netInfo != null && netInfo.isConnected()) {
                // Some sort of connection is open, check if server is reachable
                try {
                    URL url = new URL(serverURLS[i]);
                    //URL url = new URL("http://180.334.34.3"); // for server down testing
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setRequestProperty("User-Agent", "LokaVidya AA");
                    urlc.setRequestProperty("Connection", "close");
                    urlc.setConnectTimeout(10 * 1000);
                    urlc.connect();
                    isReachable = (urlc.getResponseCode() == 200);
                    if(isReachable==true)
                    {

                        Settings.setString(serverURLS[i]);
                    }
                    else
                    {
                        continue;
                    }
                } catch (IOException e) {
                    continue;
                }
            }
        }


    }
}
